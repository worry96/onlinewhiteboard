package server;

import remote.DrawerInterface;

public class User {
	public String ip;
	public int port;
	public String userName;
	public DrawerInterface drawer;
	
	// 
	public User(String ip,int port,String userName,DrawerInterface drawer) {
		this.ip = ip;
		this.port = port;
		this.userName = userName;
		this.drawer = drawer;
	}
}
