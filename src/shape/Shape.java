package shape;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import javax.swing.*;

public class Shape implements Serializable{

	public int x1, y1, x2, y2;
    public String shapeCategory;
    public Color color;
    public int stroke;
    public String user;
    ArrayList<Point> points = new ArrayList<Point>();
    int xMin = 0,yMin = 0,xMax =0 ,yMax = 0;
    String content;
    String font= "Times New Roman";
    public Shape() {
    };
    
    @Override
    public boolean equals(Object o){

        if (this.getClass()!=o.getClass())
        	return false;

        Shape s = (Shape) o;
        if (!this.shapeCategory.equals(s.shapeCategory))
            return false;

        if (x1!=s.x1)
            return false;

        if (x2!=s.x2)
            return false;

        if (y1!=s.y1)
            return false;

        if (y2!=s.y2)
            return false;

        if (!color.equals(s.color))
            return false;

        if (stroke!=s.stroke)
            return false;

        return true;
    }
    
    
    public void setFont(String s){
        font = s;
    }

    public void setContent(String content){
        this.content = content;
    }

    public Shape(int x1,int y1,int x2, int y2, String s, Color color, int stroke) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.shapeCategory = s;
        this.color = color;
        this.stroke = stroke;
    }


    public String toJson(){
        Gson g = new Gson();
        System.out.print(g.toJson(this));
        return g.toJson(this);
    }

    public static void main(String arg[]){

    }

    public void drawShape(Graphics g){
    }

    public void drawShape(JPanel p) {
    }

    public void highLight(){
        color = color.brighter();
        stroke += 3;
    }

    public void unhighLight(Color originalColor){
        color = originalColor;
        stroke -= 3;
    }

    public Area area(){
        return new Area(x1,y1,x2,y2);
    };
    
    public void update(Point p) {
    	this.x2 = p.x;
    	this.y2 = p.y;
    }
}
