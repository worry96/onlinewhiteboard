package shape;

public class UiUtil {
	public static byte[] encode(String res, String key) {
		  byte[] resbs = res.getBytes();
		  byte[] keybs = key.getBytes();
		  byte bkey = keybs[0];
		  for (int i=1;i<keybs.length;i++){
		   bkey = (byte) (bkey^keybs[i]);
		  }
		  for(int i=0;i<resbs.length;i++){
		   resbs[i] = (byte) (resbs[i]^bkey);
		  }
		  return resbs;
		 }
	 public static String decode(byte[] resbs, String key) {
	  byte[] keybs = key.getBytes();
	  byte bkey = keybs[0];
	  for (int i=1;i<keybs.length;i++){
	   bkey = (byte) (bkey^keybs[i]);
	  }
	  for(int i=0;i<resbs.length;i++){
	   resbs[i] = (byte) (resbs[i]^bkey);
	  }
	  return new String(resbs);
	 }
}
