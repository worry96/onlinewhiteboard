package remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import shape.*;
//import client.Shape;
//import client.Shapes;

public interface UserManagerInterface extends Remote{
	
	public boolean register(String ip,int port,String userName) throws RemoteException;
	
	public void drawToAll(Shape shape) throws RemoteException;
	
	public void messageToAll(byte[] message) throws RemoteException;
	
	
	public void drawToAll() throws RemoteException;
	
	// yao10-22
	public void deleteUser(String user) throws RemoteException;
	
	public void showUserList() throws RemoteException;
	
	public void notifyUser(String user) throws RemoteException;
	
	public void removeSelf(String user) throws RemoteException;

	public void askRegister(String myIp, int myPort, String userName) throws RemoteException;
	
	public boolean registerDelete(String ip,int port,String userName) throws RemoteException;
	
	public void removeShapes(Shapes shapes) throws RemoteException;
	
	public void closeNotifyToAll() throws RemoteException;
	
	public void openDrawToAll(Shapes shapes) throws RemoteException;
	
	public void closeAll() throws RemoteException;
	
	
	public boolean registerManager(String ip,int port,String userName) throws RemoteException;
}
