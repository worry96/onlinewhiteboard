package manager;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class UiUtil {
	
	private UiUtil() {
		
	}
	
	public static byte[] encode(String res, String key) {
		  byte[] resbs = res.getBytes();
		  byte[] keybs = key.getBytes();
		  byte bkey = keybs[0];
		  for (int i=1;i<keybs.length;i++){
		   bkey = (byte) (bkey^keybs[i]);
		  }
		  for(int i=0;i<resbs.length;i++){
		   resbs[i] = (byte) (resbs[i]^bkey);
		  }
		  return resbs;
		 }
	 public static String decode(byte[] resbs, String key) {
	  byte[] keybs = key.getBytes();
	  byte bkey = keybs[0];
	  for (int i=1;i<keybs.length;i++){
	   bkey = (byte) (bkey^keybs[i]);
	  }
	  for(int i=0;i<resbs.length;i++){
	   resbs[i] = (byte) (resbs[i]^bkey);
	  }
	  return new String(resbs);
	 }
	
	//�޸Ĵ���ͼ��
	public static void setFrameImage(JFrame jf) {
		// ��ȡ���������
		
		Toolkit tk = Toolkit.getDefaultToolkit();
		// ����·����ȡͼƬ
		URL path=UiUtil.class.getClassLoader().getResource("resource\\dict.jpg");
//		System.out.println(path);
		Image i = tk.getImage(path);
		// ����������ͼƬ
		jf.setIconImage(i);
	}
	
	// ���ô������
	public static void setFrameCenter(JFrame jf) {
		/**
		 * ˼·��
		 * A ��ȡ��Ļ�Ŀ��
		 * B ��ȡ����Ŀ��
		 * C (�� ��Ļ�Ŀ�-����Ŀ�)/2, (����Ļ�ĸ�-����ĸ�)/2 ��Ϊ�����������
		 */
		
		// ��ȡ���߶���
		Toolkit tk = Toolkit.getDefaultToolkit();
		// ��ȡ��Ļ�Ŀ�͸�
		Dimension d = tk.getScreenSize();
		double screenWidth = d.getWidth();
		double screenHigh = d.getHeight();
		
		double x = (screenWidth- jf.getWidth())/2;
		double y = (screenHigh - jf.getHeight())/2;
		jf.setLocation((int)x,(int)y);
//		jf.setBounds((int)x, (int)y, jf.getWidth(), jf.getHeight());
		
	}
	
	public static void prompt(String userName) {
		JButton btn1 = new JButton("accept");
	    JButton btn2 = new JButton("deny");
//		JButton btn = new JButton("confirm");
		JOptionPane pane = new JOptionPane(userName+" want to join in, accept or deny?", JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
		pane.setOptions(new JButton[] {btn1,btn2});
		pane.setInitialValue(btn1);
		pane.setInitialValue(btn2);
		final JDialog dialog = pane.createDialog("Warning");
		dialog.setModal(true);
		
		btn1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
				
			}
		});
		
		btn2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
				
			}
		});
		
		 
		dialog.setVisible(true);
	}
	
	public static void promptSuccess(String msg) {
		JButton btn = new JButton("confirm");
		JOptionPane pane = new JOptionPane(msg, JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_CANCEL_OPTION);
		pane.setOptions(new JButton[] {btn});
		pane.setInitialValue(btn);
		final JDialog dialog = pane.createDialog("Success");
		dialog.setModal(true);
		
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
			}
		});
		 
		dialog.setVisible(true);
	}
	
	
	
	
}