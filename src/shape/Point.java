package shape;

import java.io.Serializable;

public class Point implements Serializable{
    protected int x;
    protected int y;
    public Point(int x,int y){
        this.x = x;
        this.y = y;
    }
    
    public boolean equals(Object o){
        if (this.getClass()!=o.getClass())
                return false;
        Point p = (Point) o;
        return x==p.x&&y==p.y;
    }
}
