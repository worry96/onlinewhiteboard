package shape;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;


public class Blank extends FillRect {
    public Blank() {
        super(0,0,10000,10000,"blank",Color.white);
        this.shapeCategory = "Blank";
    };

    
    public Blank(int width, int height) {
        super(0,0,width,height,"blank",Color.white);
        this.shapeCategory = "Blank";
    };
    
    public Blank(int x1, int y1, int x2, int y2, String name, Color color) {
        super(x1, y1, x2, y2, name, color);
        this.shapeCategory = "Blank";
    }

    public void drawShape(JPanel p){
        Graphics g = p.getGraphics();
        g.setColor(Color.white);
        g.fillRect(0,0,p.getWidth(),p.getHeight());
//        p.getGraphics().drawRect(0,0,p.getWidth(),p.getHeight());
    }
    
    public boolean equals(Object o) {
    	if (o instanceof Blank) {
    		Blank shape = (Blank)o;
    		if (this.color.equals(shape.color)) {
    			if (this.x1==shape.x1 && this.x2==shape.x2 && this.y1==shape.y1 && this.y2==shape.y2) {
    				return true;
    			}
    		}
    	} 
    	return false;
    }

}