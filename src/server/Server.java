package server;

import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

//import javax.rmi.CORBA.Util;
import javax.swing.JOptionPane;

import remote.DrawerInterface;
import remote.UserManagerInterface;
//import client.Shape;
//import client.Shapes;
import shape.*;

public class Server extends UnicastRemoteObject implements UserManagerInterface{
	
	public static void main(String[] args) {
		
		try {
			int port = 1099;
			if (args.length !=0) {
				port = Integer.parseInt(args[0]);; 
			}
			UserManagerInterface userManager = new Server();
			LocateRegistry.createRegistry(port);
			Registry registry = LocateRegistry.getRegistry(port);
			registry.bind("manager", userManager);
			System.out.println("serverport:"+ port);
		} catch (RemoteException e) {
			System.out.println(e.getMessage());
//			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			System.out.println(e.getMessage());
//			e.printStackTrace();
		}catch (Exception e) {
		}
	}
	
	private ArrayList<User> userList = new ArrayList<User>();
	private Shapes shapes = new Shapes();
	
	public void removeShapes(Shapes shapes) throws RemoteException{
		for (User user:userList) {
			//System.out.println("drow to one user");
			System.out.println("manager shapes num:"+shapes.size());
			user.drawer.removeShapes(shapes);
		}
	}
	
	private User  deleteUser;
	public Server() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean registerDelete(String ip, int port, String userName) throws RemoteException {
		
		// register
		DrawerInterface drawer;
		try {
			drawer = (DrawerInterface)LocateRegistry.getRegistry(ip, port).lookup("drawer");
//			drawer.notifyUser("Your request to enter was denied");
			
			User user = new User(ip,port,userName,drawer);
			deleteUser = user;
////			userList.add(user);
//			user.drawer.notifyUser("Your request to enter was denied");
			
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	
	@Override
	public boolean registerManager(String ip,int port,String userName) throws RemoteException {
		// TODO Auto-generated method stub
		try {
			
			// register
			DrawerInterface drawer = (DrawerInterface)LocateRegistry.getRegistry(ip, port).lookup("drawer");
			User user = new User(ip,port,userName,drawer);
			if (userList.size() !=0) {
				user.drawer.notifyUser("There can only be one manager!");
				return false;
			}
			userList.add(user);
			//user.drawer.drawShapes(shapes);
			System.out.println("user number: "+userList.size());
			
			// message to all
			messageToAll(UiUtil.encode(("welcome, "+ userName+"\n"), "123"));
			
			ArrayList<String> names = new ArrayList<String>();
			for (User u : userList) {
				names.add(u.userName);
			}
			
			// 调用客户端的showUserList
			for (User u:userList) {
				// 在文本框中加上userList.
				u.drawer.showUserList(names);
			}
			
			return true;
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	@Override
	public boolean register(String ip,int port,String userName) throws RemoteException {
		// TODO Auto-generated method stub
		try {
			
			// register
			DrawerInterface drawer = (DrawerInterface)LocateRegistry.getRegistry(ip, port).lookup("drawer");
			User user = new User(ip,port,userName,drawer);
			userList.add(user);
			//user.drawer.drawShapes(shapes);
			System.out.println("user number: "+userList.size());
			
			// message to all
			messageToAll(UiUtil.encode(("welcome, "+ userName+"\n"), "123"));
			
			ArrayList<String> names = new ArrayList<String>();
			for (User u : userList) {
				names.add(u.userName);
			}
			
			// 调用客户端的showUserList
			for (User u:userList) {
				// 在文本框中加上userList.
				u.drawer.showUserList(names);
			}
			
			return true;
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
//	public 
	

	@Override
	public void drawToAll(Shape shape) throws RemoteException {
		shapes.add(shape);
		for (User user:userList) {
			//System.out.println("drow to one user");
			
			System.out.println("manager shapes num:"+shapes.size());
			user.drawer.drawShapes(shape);
		}
	}
	
	
	// yao
	@Override
	public void messageToAll(byte[] message) throws RemoteException {
		String tmp=UiUtil.decode(message, "123");
		for (User user:userList) {
			System.out.println("message to one user");
			user.drawer.showMessage(tmp);
			
		}
	}
	
	public void showUserList() throws RemoteException {
		ArrayList<String> names = new ArrayList<String>();
		for (User u : userList) {
			names.add(u.userName);
		}
		
		// 调用客户端的showUserList
		for (User u:userList) {
			// 在文本框中加上userList.
			u.drawer.showUserList(names);
		}
	}
	
	
	// 删除用户
	//	public void deleteUSer
	// 每个用户有一个方法deleteNotification, 
	// 1. showDialoge 展示删除，请尝试重新加入！
	// 2. 从列表中删除，
	// 加入用户更新，删除用户也更新
	
	
	// yao10-22
		@Override
	public void deleteUser(String user) throws RemoteException {
		// 1. delete user
		User deleteUser = null;
		for (User u: userList) {
			if (user.equals(u.userName)){
				deleteUser = u;
			}
		}
		userList.remove(deleteUser);
		this.deleteUser = deleteUser;
//		deleteUser.drawer.notifyUser("you has been removed, please try later!\n" );
		// 调用客户端的关闭方法。
	}
		
	public void removeSelf(String user) throws RemoteException {
		// 1. delete user
		User deleteUser = null;
		for (User u: userList) {
			if (user.equals(u.userName)){
				deleteUser = u;
			}
		}
		userList.remove(deleteUser);
	}
		

	@Override
	public void notifyUser(String user) throws RemoteException {
		
		try {
			// 1. you are denied, you can try later.
			deleteUser.drawer.notifyUser("you has been removed, please try later!\n" );
//			deleteUser.drawer.clearInformation();
		} catch (Exception e) {
			System.out.println("我出错了");
		}
	}
	
	
	@Override
	public void drawToAll() throws RemoteException {
		for (User user:userList) {
			//System.out.println("drow to one user");
			System.out.println("manager shapes num:"+shapes.size());
			user.drawer.drawShapes(shapes);
		}
	}

	@Override
	public void askRegister(String myIp, int myPort, String userName) {
		//
		System.out.println("进入ask");
		for (User u:userList) {
			System.out.println("jinru for");
			if (userName.equals(u.userName)){
				
				try {
					DrawerInterface drawer = (DrawerInterface)LocateRegistry.getRegistry(myIp, myPort).lookup("drawer");
					
					drawer.notifySameUser("username has exits, change one!");
					return;
				} catch (AccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NotBoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("manage 前");
		User manager = userList.get(0);
		System.out.println("-------");
		System.out.println(manager);
		try {
			manager.drawer.allowedLogin(myIp, myPort, userName);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void closeNotifyToAll() throws RemoteException {
		try {
			User mana = userList.get(0);
			for (User u:userList) {
				if (u.equals(mana)) {
					continue;
				}
				new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							u.drawer.notifyUser("manager close the draw, you has been removed\n" );
						} catch (Exception e) {
							// TODO Auto-generated catch block
							System.out.println(" manager close error!");
//							e.printStackTrace();
						}
					}
				}).start();
				
			}
			userList = new ArrayList<User>();
//			deleteUser.drawer.clearInformation();
		} catch (Exception e) {
			System.out.println("我出错了");
		}
	}
	
	@Override
	public void openDrawToAll(Shapes shapes) throws RemoteException {
		this.shapes = shapes;
		for (User user:userList) {
			//System.out.println("drow to one user");
			System.out.println("manager shapes num:"+shapes.size());
			user.drawer.drawShapes(shapes);
		}
	}

	@Override
	public void closeAll() throws RemoteException {
		System.exit(0);
	}
}
