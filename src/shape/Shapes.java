package shape;
import com.google.gson.Gson;
import manager.UiUtil;

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Shapes extends ArrayList<Shape> implements Serializable{
 /*
 private ArrayList<Shape> shapes = new ArrayList<>();

 public void add(Shape shape) {
  shapes.add(shape);
 }
 */

 public static Shapes recoverFromFile(String path){
  try
  {
   Shapes ss = new Shapes();
   //FileInputStream fis = new FileInputStream(path);
   File file= new File(path);
   InputStream in= new FileInputStream(file);
   byte[] bytes = new byte[in.available()];    //in.available()是得到文件的字节数
   in.read(bytes);
   System.out.print(bytes.length);
   String record = UiUtil.decode(bytes,"whiteboardkey");
   System.out.print(record);

   Gson g = new Gson();
   ss = g.fromJson(record,Shapes.class);
   for (int i=0;i<ss.size();i++){
    //  
    if("Line".equals(ss.get(i).shapeCategory)){
     ss.set(i,g.fromJson(ss.get(i).toJson(),Line.class));
    }
    if("Pencil".equals(ss.get(i).shapeCategory)){
     ss.set(i,g.fromJson(ss.get(i).toJson(),Curve.class));
    }
    if("Oval".equals(ss.get(i).shapeCategory)){
     ss.set(i,g.fromJson(ss.get(i).toJson(),Oval.class));
    }
    if("Text".equals(ss.get(i).shapeCategory)){
     ss.set(i,g.fromJson(ss.get(i).toJson(), Text.class));
    }
    if("Eraser".equals(ss.get(i).shapeCategory)){
     ss.set(i,g.fromJson(ss.get(i).toJson(),Eraser.class));
    }
    if("Rectangle".equals(ss.get(i).shapeCategory)){
     ss.set(i,g.fromJson(ss.get(i).toJson(),Rect.class));
    }
   }
   return ss;

  }catch(IOException e){
   System.out.println("Wrong path, Please make sure the path is correct.");
   return null;
  }
  catch (Exception e){
   e.printStackTrace();
   System.out.println("Wrong file content.");
   return null;
  }

 }

 public String saveinFile(String fileName){
  try {
   FileOutputStream fos = new FileOutputStream(fileName+".wb");
   System.out.print(new Gson().toJson(this));
   fos.write(UiUtil.encode(new Gson().toJson(this),"whiteboardkey"));
   fos.close();
   return "file saved successfully";
  } catch(IOException e) { return "Failed to save data";}
 }

 public void drawShape(Graphics g){
  for(int i=0;i<size();i++){
   get(i).drawShape(g);
  }
 }
 
 public void removeShapes(Shapes shapes) {
  for (Shape shape:shapes) {
   this.remove(shape);
  }
 }
}