package client;

//import com.sun.tools.javac.jvm.Items;

import javax.swing.*;


import remote.*;

//import javax.xml.bind.JAXBPermission;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import shape.*;

// Listener
public class Drawer implements MouseListener, MouseMotionListener, ActionListener, WindowListener {
	//paintBasedOnShapes(Shape shape): paint shape on the shapes
	String managerIp = "localhost";
	String myIp = "localhost";
	String deleteMessage = "you are not in the whiteboard, try to join one!";
	String unknownError = "error happen, try later";
	public void InitialDrawing() {
		try {
			remoteManager.drawToAll();
		} catch (NullPointerException e1) {
			JOptionPane.showMessageDialog(p, e1.getMessage());
		} catch (RemoteException e) {
			shapes = new Shapes();
			drawBasedOnShapes(null);
			JOptionPane.showMessageDialog(p, connectionError);
			System.exit(0);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(p, unknownError);
			System.exit(0);
		}
	}
	
	public Drawer( JTextArea  record, JList list) {
//		try {
		this.record =record;
		this.list = list;
		// 10.13.248.32
		// port号范围： 2000-60000
//		int port =(int) ((Math.random()*2000)+2000);
//		System.out.println(port);
//			registerToManager(port,managerIp,1099);
			
//		} catch (RemoteException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	protected Graphics graphics;
    private String name;
    private JColorChooser colorChooser;
    public JPanel p;
    //private JPanel tmpPanel;
    private Shape tmpShape;
	protected Shapes shapes;
	protected String currentFont;
	private int selectX = -1;
	private int selectY = -1;
	protected int stroke=5;
    private Shapes selectedShapes;
    private ArrayList<Color> originalColors;

    private DrawerInterface remoteDrawer; 
	public UserManagerInterface remoteManager;
	public String connectionError = "connection error, try later!";
	//yao
	JTextArea jtext_chat;
	JButton sendButton;
	JTextArea record;
	String username = "username";
	JList list;
	String userName;
	
//    public void setTmpPanel(JPanel p) {
//    	this.tmpPanel = p;
//    	Graphics g = tmpPanel.getGraphics();
//    	g.setColor(Color.white);
//    	g.fillRect(0, 0, p.getWidth(), p.getHeight());
//    }
    
	
	public void registerToManager(int myPort, String managerIp, int managerPort, String userName) throws Exception{
		remoteDrawer = new RemoteDrawer(this);
//		try {
			LocateRegistry.createRegistry(myPort);
			
			Registry registry = LocateRegistry.getRegistry(myPort);
			
			registry.bind("drawer", this.remoteDrawer);
			this.userName = userName;
//			try {
				remoteManager = (UserManagerInterface) LocateRegistry.getRegistry(managerIp, managerPort).lookup("manager");
//			}
//			catch (ConnectException e) {
//				JOptionPane.showMessageDialog(p, "connection error, try later");
//				return;
//			}
			
			// 10.13.248.32
//			int number = (int) (Math.random()*1000+100);
//			userName = "userName"+number;
			// 改成首先askRegister
//			try {
				myIp = InetAddress.getLocalHost().getHostAddress();
				System.out.println(myIp);
				
//			} catch (UnknownHostException e) {
//				JOptionPane.showMessageDialog(p, e.getMessage());
//			}
			remoteManager.askRegister(myIp, myPort, userName);
			
//		} catch (RemoteException | AlreadyBoundException | NotBoundException e) {
//			JOptionPane.showMessageDialog(p, e.getMessage());
//			e.printStackTrace();
//		} 
	}
    
    public void setPanel(JPanel p){
        this.p = p;
    }
    
    private void drawBasedOnShapes(Shape shape) {
    	
    	Image image = getStableImage();
    	if (shape!=null) {
    		Graphics g2 = image.getGraphics();
    		g2.setColor(getColor());
    		shape.drawShape(g2);
    	}
		p.getGraphics().drawImage(image, 0, 0, p.getWidth(), p.getHeight(), p);
    }
    
    private Image getStableImage() {
    	Image image = p.createImage(p.getWidth(), p.getHeight());
    	
    	Graphics g2 = image.getGraphics();
    	
    	g2.setColor(Color.white);
    	g2.fillRect(0, 0, p.getWidth(), p.getHeight());
    	g2.setColor(getColor());
    	shapes.drawShape(g2);
    	
    	return image;
    	
    }
    
//    private void addShapeToImage(Shape shape) {
//    	Image image = getStableImage();
//    	Graphics g2 = image.getGraphics();
//    	g2.setColor(getColor());
//    	shape.drawShape(g2);
//    	saveImage(image);
//    }
    
//    private void saveImage(Image image) {
//    	tmpPanel.getGraphics().drawImage(image,tmpPanel.getWidth(),tmpPanel.getWidth(),null);
//    }
    
    public void addShape(Shape shape) {
    	shapes.add(shape);
    	drawBasedOnShapes(tmpShape);
    	System.out.println("client shapes num:"+shapes.size());
    }
    
    
    public void setShapes(Shapes sp) {
        this.shapes = sp;
        //System.out.println("drawShapes:"+shapes.size());
        drawBasedOnShapes(tmpShape);
    }

    public void mouseClicked(java.awt.event.MouseEvent e) {

    }

    public void mousePressed(java.awt.event.MouseEvent e) {
        {
        	
        	
        	Toolkit tk = Toolkit.getDefaultToolkit();
          	 Image image = new ImageIcon(getClass().getResource("/eraser 1.png")).getImage();
          	 Image image_pencil = new ImageIcon(getClass().getResource("/Pencil.png")).getImage();
//          	 Image image_text = new ImageIcon(getClass().getResource("/Text.png")).getImage();
          	  Cursor cursor_eraser = tk.createCustomCursor(image, new java.awt.Point(15, 25), "high"); 
          	  Cursor cursor_pencil = tk.createCustomCursor(image_pencil, new java.awt.Point(20, 20), "norm"); 
//          	  Cursor cursor_text = tk.createCustomCursor(image_text, new java.awt.Point(10, 10), "norm");
          	  
            if(selectedShapes != null && originalColors!= null){
                for(int i=0;i<selectedShapes.size();i++) {
                    selectedShapes.get(i).unhighLight(originalColors.get(i));
                }
                selectedShapes.clear();
                originalColors.clear();
            }

            if ("Line".equals(name)) {
            	//System.out.println(getColor());
            	tmpShape = new Line(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            	p.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            }
            
            if ("Rectangle".equals(name)) {
            	tmpShape = new Rect(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            	p.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            }
            
            if ("Oval".equals(name)) {
            	tmpShape = new Oval(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            	p.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
            }
            
            if ("Pencil".equals(name)) {

            	tmpShape = new Curve(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            	p.setCursor(cursor_pencil);
            }
            
            if ("Eraser".equals(name)) {
            	tmpShape = new Eraser(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
            	p.setCursor(cursor_eraser);
            }

            if ("Text".equals(name)){
                tmpShape = new Text(e.getX(),e.getY(),e.getX(),e.getY(),name, getColor(),getStroke());
//                p.setCursor(cursor_text);
                String str=JOptionPane.showInputDialog(p,"Enter text input","Confirm",JOptionPane.PLAIN_MESSAGE);
                if (str != null) {
                	tmpShape.setContent(str);
                    tmpShape.setFont(currentFont);
                    tmpShape.drawShape(p);
                    //shapes.add(tmpShape);
                    try {
    					remoteManager.drawToAll(tmpShape);
    				} catch (NullPointerException e1) {
    					JOptionPane.showMessageDialog(p, deleteMessage);
    				} catch (RemoteException e1) {
    					shapes = new Shapes();
    					drawBasedOnShapes(null);
    					JOptionPane.showMessageDialog(p, connectionError);
    					System.exit(0);
    				} catch (Exception e1) {
    					JOptionPane.showMessageDialog(p, unknownError);
    					System.exit(0);
    				}
                }
                
                tmpShape = null;
            }
            if ("Select".equals(name)){
                selectX = e.getX();
                selectY = e.getY();
                tmpShape = new Rect(e.getX(),e.getY(),e.getX(),e.getY(),name, Color.black,1);
            }

        }
    }

    public void mouseReleased(java.awt.event.MouseEvent e) {
        {
        	p.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            if ("Line".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	//shapes.add(tmpShape);
            	
            	
            	try {
					remoteManager.drawToAll(tmpShape);
					tmpShape = null;
				}catch (NullPointerException e1) {
					JOptionPane.showMessageDialog(p, deleteMessage);
				} 
            	catch (RemoteException e1) {
            		shapes = new Shapes();
            		drawBasedOnShapes(null);
					JOptionPane.showMessageDialog(p, connectionError);
					System.exit(0);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(p, unknownError);
					System.exit(0);
				}
            	//drawBasedOnShapes(null);
            }

            /*
            if ("Text".equals(name)){
                tmpShape.update(new Point(e.getX(),e.getY()));
                shapes.add(tmpShape);
                tmpShape.drawShape(p);
                tmpShape = null;
            }*/
            
            if ("Rectangle".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	//shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	
            	
            	try {
					remoteManager.drawToAll(tmpShape);
					tmpShape = null;
				}catch (NullPointerException e1) {
					JOptionPane.showMessageDialog(p, deleteMessage);
				}  
            	catch (RemoteException e1) {
            		shapes = new Shapes();
            		drawBasedOnShapes(null);
					JOptionPane.showMessageDialog(p, connectionError);
					System.exit(0);
				}catch (Exception e1) {
					JOptionPane.showMessageDialog(p, unknownError);
					System.exit(0);
				}
            	//drawBasedOnShapes(null);
            }
            
            if ("Oval".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	//shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	
            	
            	try {
					remoteManager.drawToAll(tmpShape);
					tmpShape = null;
				} catch (NullPointerException e1) {
					JOptionPane.showMessageDialog(p, deleteMessage);
				} catch (RemoteException e1) {
					shapes = new Shapes();
					drawBasedOnShapes(null);
					JOptionPane.showMessageDialog(p, connectionError);
					System.exit(0);
				}
            	//drawBasedOnShapes(null);
            }
            
            if ("Pencil".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	//shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	
            	
            	try {
					remoteManager.drawToAll(tmpShape);
					tmpShape = null;
				} catch (NullPointerException e1) {
					JOptionPane.showMessageDialog(p, deleteMessage);
				} catch (RemoteException e1) {
					shapes = new Shapes();
					drawBasedOnShapes(null);
					JOptionPane.showMessageDialog(p, connectionError);
					System.exit(0);
//					e1.printStackTrace();
				}catch (Exception e1) {
					JOptionPane.showMessageDialog(p, unknownError);
					System.exit(0);
				}
            	//drawBasedOnShapes(null);
            }

            
            if ("Eraser".equals(name)) {
            	tmpShape.update(new Point(e.getX(),e.getY()));
            	//shapes.add(tmpShape);
            	//addShapeToImage(tmpShape);
            	
            	
            	try {
					remoteManager.drawToAll(tmpShape);
					tmpShape = null;
				} catch (NullPointerException e1) {
					JOptionPane.showMessageDialog(p, deleteMessage);
				} catch (RemoteException e1) {
					shapes = new Shapes();
					drawBasedOnShapes(null);
					JOptionPane.showMessageDialog(p, connectionError);
					System.exit(0);
				}catch (Exception e1) {
					JOptionPane.showMessageDialog(p, unknownError);
					System.exit(0);
				}
            	//drawBasedOnShapes(null);
            }


            if ("Select".equals(name)){
                selectedShapes = new Shapes();
                originalColors = new ArrayList<>();
                if (selectX!=-1 &&selectY != -1){
                    Area select = new Area(selectX,selectY,e.getX(),e.getY());
                    for(int i=0;i<shapes.size();i++){
                        if (shapes.get(i).area().coveredBy(select)){
                            originalColors.add(shapes.get(i).color);
                            shapes.get(i).highLight();
                            selectedShapes.add(shapes.get(i));
                        }
                    }
                    /*
                    for(int i=0;i<selectedShapes.size();i++){
                        shapes.remove(selectedShapes.get(i));
                    }
                    */
                    repaint(p);
                }
            }


        }
    }

    public void mouseDragged(java.awt.event.MouseEvent e) {
        // 画笔重载需注意内存
        if("Select".equals(name)){
            tmpShape.update(new Point(e.getX(),e.getY()));
            drawBasedOnShapes(tmpShape);
        }

    	if("Line".equals(name)) {
    		tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
    	}
    	
    	if("Rectangle".equals(name)) {
    		tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
   		}
    	
    	if ("Oval".equals(name)) {
    		tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
    	}
    	
    	if ("Pencil".equals(name)) {
    		tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
    		
        }
        if ("Eraser".equals(name)) {
        	tmpShape.update(new Point(e.getX(),e.getY()));
    		drawBasedOnShapes(tmpShape);
        }


    }


    public void mouseMoved(java.awt.event.MouseEvent e) {
        //System.out.println(e.getX()+" "+e.getY());

    }

    // 按钮的单击事件
    public void actionPerformed(ActionEvent e) {
    	if (remoteManager == null) {
    		JOptionPane.showMessageDialog(p, "connection error, try again!");
    		return;
    	}
    	
    	if (!"Choose Color".equals(e.getActionCommand()))
    		System.out.println("没");
    		name = e.getActionCommand();
    		System.out.println(name);
        if ("Clear".equals(name)) {
        	shapes.clear();
        	repaint(p);
        }
        // yao
        if ("Send".equals(name)) {
        	String message = username + ": "+ jtext_chat.getText()+"\n";
        	jtext_chat.setText("");
			// 获取信息内容 将内容置为空， 发送信息
//			sendMessage(message);
//			System.out.println("Sending message : " + message);
			try {
				// jiami
				byte[] m=UiUtil.encode(message, "123");
				
				remoteManager.messageToAll(m);
			} catch (NullPointerException e1) {
				JOptionPane.showMessageDialog(p, deleteMessage);
			} catch (RemoteException e1) {
				e1.printStackTrace();
				System.out.println("has mistake");
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(p, unknownError);
				System.exit(0);
			}
        }
        
        if ("Remove".equals(name)) {
        	
        	String deleteUserName = (String) list.getSelectedValue();
        	try {
        		// 删除用户
				remoteManager.deleteUser(deleteUserName);
				// 聊天记录通知其他人 
				remoteManager.messageToAll(UiUtil.encode((deleteUserName+" has been forced to leave\n"), "123"));
				//  用户列表更新
				remoteManager.showUserList();
			} catch (NullPointerException e1) {
				JOptionPane.showMessageDialog(p, deleteMessage);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(p, unknownError);
				System.exit(0);
			}
        	
        }
        
        
    }
    public void mouseEntered(java.awt.event.MouseEvent e) {
    }

    public void mouseExited(java.awt.event.MouseEvent e) {
    }


    public void repaint(JPanel p){
        Blank b = new Blank(p.getWidth(),p.getHeight());
        b.drawShape(p);
        //System.out.print(shapes.size());
        for(int i=0;i<shapes.size();i++){
            shapes.get(i).drawShape(p);
        }
    }

    public void setColorChooser(JColorChooser cc){
        colorChooser = cc;
    }
    
    public Color getColor(){
        return colorChooser==null?Color.black:colorChooser.getColor();
    }

    public int getStroke(){
        return stroke;
    }

    public void undo() {
    	  try {
    	   Shapes s = new Shapes();
    	   s.add(shapes.get(shapes.size()-1));
    	   remoteManager.removeShapes(s);
    	  } catch (RemoteException e) {
    	   e.printStackTrace();
    	  } catch (Exception e1) {
				JOptionPane.showMessageDialog(p, unknownError);
				System.exit(0);
    	  }
    	 }
    
    
//    public void undo() {
//        selectedShapes = new Shapes();
//        selectedShapes.add(shapes.get(shapes.size()-1));
//        delSelected();
//       }

//    public void undo() {
//    	shapes.remove(shapes.size()-1);
//    	repaint(p);
//    }

    public void delSelected(){
        if(selectedShapes == null)
            return;
//        for (int i=0; i<selectedShapes.size();i++){
//            shapes.remove(selectedShapes.get(i));
//        }
//        repaint(p);
        try {
        	tmpShape = null;
        	for (int i=0;i<selectedShapes.size();i++) {
        		selectedShapes.get(i).unhighLight(originalColors.get(i));
        	}
			remoteManager.removeShapes(selectedShapes);
		} catch (NullPointerException e1) {
			JOptionPane.showMessageDialog(p, deleteMessage);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e1) {
			JOptionPane.showMessageDialog(p, unknownError);
			System.exit(0);
		}
    }



	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		try {
			if (remoteManager != null) {
				remoteManager.messageToAll(UiUtil.encode((userName+" has leaved"), "123"));
				remoteManager.removeSelf(userName);
				remoteManager.showUserList();
				
			}
			else {
				System.exit(0);
			}
//		} catch (NullPointerException e1) {
//			JOptionPane.showMessageDialog(p, deleteMessage);
		} catch (RemoteException e1) {
			e1.printStackTrace();
			System.out.println("has mistake");
		}catch (Exception e1) {
			JOptionPane.showMessageDialog(p, unknownError);
			System.exit(0);
		}
	}


	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}


//    public void savePic(Image iamge){
//        int w = iamge.getWidth(p);
//        int h = iamge.getHeight(p);
//
////首先创建一个BufferedImage变量，因为ImageIO写图片用到了BufferedImage变量。
//        BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_3BYTE_BGR);
//
////再创建一个Graphics变量，用来画出来要保持的图片，及上面传递过来的Image变量
//        Graphics g = bi.getGraphics();
//        try {
//            g.drawImage(iamge, 0, 0, null);
//
////将BufferedImage变量写入文件中。
//            ImageIO.write(bi,"jpg",new File("d:/gray11.jpg"));
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
//    }

	public void removeShapes(Shapes shapes) {

		this.shapes.removeShapes(shapes);
		System.out.println("shapes size:"+this.shapes.size());
		if (tmpShape == null) {
			System.out.println("no tmpshape");
		}
		this.drawBasedOnShapes(tmpShape);
	}
}
