package remote;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import shape.*;


public interface DrawerInterface extends Remote{
	
	public void drawShapes(Shapes shapes) throws RemoteException;
	
	public void drawShapes(Shape shape) throws RemoteException;
	
	//public void registerToManager(int myPort,String managerIp,int managerPort) throws RemoteException;
	
	public void showMessage(String message) throws RemoteException;

	public void showUserList(ArrayList<String> names) throws RemoteException;

	public void notifyUser(String string) throws RemoteException;
	
	
	//
	public void allowedLogin(String myIp, int myPort, String userName)  throws RemoteException;
	
	public void removeShapes(Shapes shapes) throws RemoteException;
	
	public void notifySameUser(String message) throws RemoteException;
	
}
