package manager;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import remote.DrawerInterface;
import remote.ManagerDrawerInterface;
import shape.*;
public class RemoteDrawer extends UnicastRemoteObject implements ManagerDrawerInterface{
	private Drawer drawer;
	private ManagerUI clientUI;
	protected RemoteDrawer() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	protected RemoteDrawer(Drawer drawer) throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
		this.drawer = drawer;
	}
	
	@Override
	public void drawShapes(Shape shape) throws RemoteException {
		// TODO Auto-generated method stub
		drawer.addShape(shape);
		
	}
	
	@Override
	public void drawShapes(Shapes shapes) throws RemoteException {
		// TODO Auto-generated method stub
		drawer.setShapes(shapes);
		
	}

	// yao
	@Override
	public void showMessage(String message) throws RemoteException {
		// TODO Auto-generated method stub
//		clientUI.showMessage(message);
		drawer.record.append(message);
		
	}
	
	@Override
	public void showUserList(ArrayList<String> names) throws RemoteException {
		
		String[] array=new String[names.size()];  
        for(int i=0;i<names.size();i++){  
            array[i]=(String)names.get(i);  
        }  
		drawer.list.setListData(array);
	}
	
	// yao10-22
	@Override
	public void notifyUser(String message) throws RemoteException {
		try {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						System.out.println("something wrong!");
						e.printStackTrace();
					}
					JOptionPane.showMessageDialog(drawer.p, message);
					System.exit(0);
				}
			}).start();
			
		} catch (Exception e) {
			System.out.println("通知出错!");
		}
	}

	@Override
	public void allowedLogin(String myIp, int myPort, String userName) throws RemoteException {
		drawer.isAllowedLogin(myIp, myPort,  userName);
		
		// TODO Auto-generated method stub
		
	}
	
//	public void registerToManager(int myPort,String managerIp,int managerPort) {
//		try {
//			LocateRegistry.createRegistry(myPort);
//			Registry registry = LocateRegistry.getRegistry(myPort);
//			registry.bind("drawer", this);
//			
//		} catch (RemoteException | AlreadyBoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
//	public void registerToManager(int myPort,String managerIp,int managerPort) {
//		try {
//			LocateRegistry.createRegistry(myPort);
//			Registry registry = LocateRegistry.getRegistry(myPort);
//			registry.bind("drawer", this);
//			
//		} catch (RemoteException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	public void removeShapes(Shapes shapes) {
		drawer.removeShapes(shapes);
	}

	@Override
	public void notifySameUser(String message) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
}
