package shape;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ColorUIResource;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.Serializable;

public class Text extends Shape implements Serializable{

    public Text(int x1, int y1, int x2, int y2, String s, Color color, int stroke) {
        super(x1,y1,x2,y2,s,color,stroke);
    }

    @Override
    public boolean equals(Object o){
        if(this.getClass()!=o.getClass())
            return false;
        Text s = (Text) o;
        if (!this.shapeCategory.equals(s.shapeCategory))
            return false;
        if (x1!=s.x1)
            return false;
        if (x2!=s.x2)
            return false;
        if (y1!=s.y1)
            return false;
        if (y2!=s.y2)
            return false;
        if (!color.equals(s.color))
            return false;
        if (stroke!=s.stroke)
            return false;
        if(font!=null)
        	if(s.font==null)
        		return false;
        if(s.font!=null)
        	if(font==null)
        		return false;
        if(s.font!=null && font!=null)
        	if(!font.equals(s.font))
        		return false;
        if (!content.equals(s.content))
            return false;
        return true;
    }
    public void drawShape(JPanel p){
        Graphics g = p.getGraphics();
        drawShape(g);
//        p.validate();
    }

    @Override
    public void highLight(){
        color = color.brighter();
    }

    @Override
    public void unhighLight(Color originalColor){
        color = originalColor;
    }

    public Area area(){
        return new Area(x1,y1-(int)Math.round(stroke*3.7+3.2), x2+(int)(Math.round(stroke*3.7+3.2))*content.length()/2,y1);
    }

    public void drawShape(Graphics g){
        if (content == null)
            return;
        g.setColor(color);
        Font f;
        if(font != null)
            f = new Font(font, 0, 8+stroke*5);
        else f = new Font("Times New Roman", 0, 8+stroke*5);
        g.setFont(f);
        g.drawString(content,x1,y1);
    }

}