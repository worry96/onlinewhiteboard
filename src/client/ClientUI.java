package client;

import java.awt.*;
import shape.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.rmi.RemoteException;

public class ClientUI extends JPanel{

    private JFrame frame;
    private Drawer drawer;
    private static final long serialVersionUID = 1L;
    private Shapes shapeParameter = new Shapes();
    public int width = Toolkit.getDefaultToolkit().getScreenSize().width;
    public int height = Toolkit.getDefaultToolkit().getScreenSize().height;
    public int windowsWidth = 1000;
    public int windowsHeight = 700;
//    private ImageIcon[] images;
    private String[] shapeArray = { "Select","Line", "Rectangle", "Oval", "Text", "Pencil", "Eraser" };
//    private String savePath= null;
    private ImageIcon[] images = new ImageIcon[7];
    private String savePath= null;
    
    //yao
//    JTextArea chatBox;
//    JButton sendButton;
    
    Box sliderBox = new Box(BoxLayout.Y_AXIS);  
    JTextField showVal = new JTextField();  
    ChangeListener listener;  
    
    public static String manageIP;
    public static int managePort;
    public static String userName;
    
    public void loadImages(){
        for (int i = 0; i < images.length; i++){
            images[i] = createImageIcon( "/images/" + shapeArray[i] + ".png");
        }
    }

    /** Returns an ImageIcon, or null if the path was invalid. */
    public static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = ClientUI.class.getResource(path);

        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
    
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
            	
            	try {
                	manageIP = args[0];
                    managePort = Integer.parseInt(args[1]);
                    userName = args[2];
				} catch (Exception e) {
					System.out.println(" please provide correct ip, port and username");
					System.exit(0);
				}

                try {
                    ClientUI window = new ClientUI();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                	System.out.println("has wrong, try later!");
//                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public ClientUI() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
    	
    	  //定义一个监听器，用于监听所有滑动条  
        listener = new ChangeListener()  
        {    
            public void stateChanged(ChangeEvent event)  
            {    
                //取出滑动条的值，并在文本中显示出来  
                JSlider source = (JSlider) event.getSource();  
                showVal.setText("当前滑动条的值为：" + source.getValue());  
            }  
        };  
    	
        
        frame = new JFrame("White Board");
//        frame.setSize(windowsWidth, windowsHeight);
        // 设置窗体大小
        frame.setBounds((width - windowsWidth) / 2,(height - windowsHeight) / 2, windowsWidth, windowsHeight);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        gridBagLayout.columnWeights = new double[]{0.5, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,1.0,1.0,0.5,0.1,0.1};
        gridBagLayout.rowWeights = new double[]{0.5, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,1.0};
        frame.getContentPane().setLayout(gridBagLayout);
        
        
//		JPanel panel_2 = new JPanel();
        GridBagConstraints gbc_panel_2 = new GridBagConstraints();
        gbc_panel_2.gridheight = 13;
        gbc_panel_2.gridwidth = 8;
        gbc_panel_2.insets = new Insets(0, 0, 5, 5);
        gbc_panel_2.fill = GridBagConstraints.BOTH;
        gbc_panel_2.gridx = 2;
        gbc_panel_2.gridy = 1;
        this.setPreferredSize(new Dimension(900, 700));
        this.setBackground(Color.white);
        frame.getContentPane().add(this, gbc_panel_2);
        
        //------------------------------
        JPanel panel_chat = new JPanel();
        panel_chat.setBackground(Color.LIGHT_GRAY);
        GridBagConstraints gbc_panel_chat = new GridBagConstraints();
        gbc_panel_chat.gridheight = 13;
        gbc_panel_chat.gridwidth = 4;
        gbc_panel_chat.insets = new Insets(0, 0, 5, 5);
        gbc_panel_chat.fill = GridBagConstraints.BOTH;
        gbc_panel_chat.gridx = 10;
        gbc_panel_chat.gridy = 1;
        frame.getContentPane().add(panel_chat, gbc_panel_chat);
        

        GridBagLayout gbl_panel_chat = new GridBagLayout();
        gbl_panel_chat.columnWidths = new int[]{0,0,0,0};
        gbl_panel_chat.rowHeights = new int[]{0,0,0,0,0,0,0,0,0,0,0,0,0};
        gbl_panel_chat.columnWeights = new double[]{1.0,1.0,1.0,0.005};
        gbl_panel_chat.rowWeights = new double[]{0.01,1.0,1.0,1.0,1.0,0.005,1.0,1.0,1.0,1.0,1.0,1.0,0.05};
        panel_chat.setLayout(gbl_panel_chat);
        
        JLabel jl_client = new JLabel("Client List");
        GridBagConstraints gbc_jl_client = new GridBagConstraints();
        gbc_jl_client.insets = new Insets(0,0,5,0);
        gbc_jl_client.gridx = 0;
        gbc_jl_client.gridy = 0;
        panel_chat.add(jl_client,gbc_jl_client);
        
        JList list = new JList();
        GridBagConstraints gbc_list = new GridBagConstraints();
        gbc_list.gridwidth = 4;
        gbc_list.gridheight = 4;
        gbc_list.fill = GridBagConstraints.BOTH;
        gbc_list.gridx = 0;
        gbc_list.gridy = 1;
        frame.getContentPane().add(list, gbc_list); 
        panel_chat.add(list, gbc_list);
        
        
        //
//        JPanel panel_record = new JPanel();
        JTextArea record = new JTextArea();
//        record.setPreferredSize(new Dimension(300,500));
        GridBagConstraints gbc_panel_record = new GridBagConstraints();
        gbc_panel_record.gridwidth = 4;
        gbc_panel_record.gridheight = 5;
        gbc_panel_record.insets = new Insets(0, 0, 5, 5);
        gbc_panel_record.fill = GridBagConstraints.BOTH;
        gbc_panel_record.gridx = 0;
        gbc_panel_record.gridy = 6;
//        frame.getContentPane().add(panel_record, gbc_panel_record);
        record.setEditable(false);
        record.setBackground(Color.white);
//        record.setLineWrap(true);
        record.setLineWrap(true);
        record.setWrapStyleWord(true);
        
        JScrollPane scrollPane_record = new JScrollPane(record);
        panel_chat.add(scrollPane_record, gbc_panel_record);
        
        
        // 创建事件监听器对象 record需要优先于register前，
        drawer = new Drawer(record, list);
        //yao 
        // 给画布添加监听器
        this.addMouseListener(drawer);
        this.addMouseMotionListener(drawer);

        // 设置窗体可见
        frame.setVisible(true);
        
        
        frame.addWindowListener(drawer);
        
        //get tmpPanel
        GridBagConstraints gbc_panel_tmp = new GridBagConstraints();
        gbc_panel_tmp.gridheight = 13;
        gbc_panel_tmp.gridwidth = 8;
        gbc_panel_tmp.insets = new Insets(0, 0, 5, 5);
        gbc_panel_tmp.fill = GridBagConstraints.BOTH;
        gbc_panel_tmp.gridx = 2;
        gbc_panel_tmp.gridy = 1;
        JPanel tmpPanel = new JPanel();
        tmpPanel.setPreferredSize(new Dimension(900, 700));
        tmpPanel.setBackground(Color.WHITE);
        
        tmpPanel.setOpaque(false);

        
        frame.getContentPane().add(tmpPanel, gbc_panel_tmp);

     // 获取画笔
        Graphics g = this.getGraphics();
        drawer.setPanel(this);
        //drawer.setTmpPanel(tmpPanel);
        // 将画笔传递过去
        //drawer.setGr(g.create(0,0,this.width,this.height));
        //drawer.setGr(g);
        // 将图形数组传递过去
        drawer.setShapes(shapeParameter);
        
        // remove按钮
        JButton but_remove = new JButton("Remove");
        GridBagConstraints gbc_but_remove = new GridBagConstraints();
        gbc_but_remove.insets = new Insets(0,0,5,0);
        gbc_but_remove.gridx = 3;
        gbc_but_remove.gridy = 5;
        gbc_but_remove.fill = GridBagConstraints.CENTER ;
//        gbc_but_remove.gridwidth = 4;
        panel_chat.add(but_remove, gbc_but_remove);
        but_remove.addActionListener(drawer);
        but_remove.setVisible(false);
        
        JLabel jl_chat_record = new JLabel("chat record");
        GridBagConstraints gbc_jl_chat_record = new GridBagConstraints();
        gbc_jl_chat_record.insets = new Insets(0,0,5,0);
        gbc_jl_chat_record.gridx = 0;
        gbc_jl_chat_record.gridy = 5;
        gbc_jl_chat_record.fill = GridBagConstraints.CENTER ;
        panel_chat.add(jl_chat_record, gbc_jl_chat_record);
        

        JTextArea jtext_chat = new JTextArea();
        
//        jtext_chat.setPreferredSize(new Dimension(150,80));
        GridBagConstraints gbc_text_chat = new GridBagConstraints();
        gbc_text_chat.gridwidth = 3;
        gbc_text_chat.gridheight = 2;
        gbc_text_chat.insets = new Insets(0, 0, 0, 0);
        gbc_text_chat.fill = GridBagConstraints.BOTH;
        gbc_text_chat.gridx = 0;
        gbc_text_chat.gridy = 11;
//        frame.getContentPane().add(panel_record, gbc_panel_record);
        jtext_chat.setBackground(Color.white);
//        jtext_chat.append("\n\r");
        jtext_chat.setLineWrap(true);
        jtext_chat.setWrapStyleWord(true);
        panel_chat.add(new JScrollPane(jtext_chat), gbc_text_chat);
        
       
        JButton but_send = new JButton("Send");
        GridBagConstraints gbc_but_send = new GridBagConstraints();
        gbc_but_send.insets = new Insets(0,0,5,0);
        gbc_but_send.gridx = 3;
        gbc_but_send.gridy = 12;
        gbc_but_send.fill = GridBagConstraints.CENTER ;

        panel_chat.add(but_send, gbc_but_send);

        
        JPanel panel_1 = new JPanel();

        panel_1.setBackground(Color.LIGHT_GRAY);
        GridBagConstraints gbc_panel_1 = new GridBagConstraints();
        gbc_panel_1.gridheight = 5;
        gbc_panel_1.gridwidth = 2;
        gbc_panel_1.insets = new Insets(0, 0, 5, 5);
        gbc_panel_1.fill = GridBagConstraints.BOTH;
        gbc_panel_1.gridx = 0;
        gbc_panel_1.gridy = 8;
        frame.getContentPane().add(panel_1, gbc_panel_1);
        
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[]{0,0};
        gbl_panel_1.rowHeights = new int[]{0,0,0,0,0};
        gbl_panel_1.columnWeights = new double[]{1.0,0.001};
        gbl_panel_1.rowWeights = new double[]{0.5,0.5,1.0,0.5,1.0};
        panel_1.setLayout(gbl_panel_1);
        
        JButton jbutton = new JButton("Choose Color");  //按钮，单击响应事件，打开文件选择器
        jbutton.setIcon(new ImageIcon(ClientUI.class.getResource("/colorchooser.png")));
        GridBagConstraints gbc_jbu = new GridBagConstraints();
        gbc_jbu.insets = new Insets(0,0,5,0);
        gbc_jbu.gridx = 0;
        gbc_jbu.gridy = 0;
        gbc_jbu.gridwidth = 2;
        jbutton.addActionListener(e -> {
            JColorChooser colorChooser = new JColorChooser();
            JDialog dialog = JColorChooser.createDialog(frame, "choose color", false, colorChooser,
                    e1 -> g.setColor(colorChooser.getColor()), null);
            dialog.setVisible(true);
            jbutton.addActionListener(drawer);
            drawer.setColorChooser(colorChooser);
        });

        panel_1.add(jbutton, gbc_jbu);
        
        
        JLabel jl_username = new JLabel("Username:");
        GridBagConstraints gbc_username = new GridBagConstraints();
        gbc_username.gridx = 0;
        gbc_username.gridy = 3;
        gbc_username.insets = new Insets(0, 0, 5, 5);
        gbc_username.fill = GridBagConstraints.BOTH;
        panel_1.add(jl_username, gbc_username);
        
        JTextArea join_area = new JTextArea();
//        join_area.setPreferredSize(new Dimension(120,80));
        GridBagConstraints gbc_join = new GridBagConstraints();
        gbc_join.gridwidth = 1;
        gbc_join.gridheight = 1;
        gbc_join.insets = new Insets(0, 0, 5, 5);
        gbc_join.fill = GridBagConstraints.BOTH;
        gbc_join.gridx = 0;
        gbc_join.gridy = 4;
//        frame.getContentPane().add(panel_record, gbc_panel_record);
        
        join_area.setText(userName);
        
        record.setBackground(Color.white);
        join_area.setLineWrap(true);
        join_area.setWrapStyleWord(true);
        
        JScrollPane scrollPane = new JScrollPane(join_area);
        
        panel_1.add(scrollPane, gbc_join);
        
        
        JButton but_join = new JButton("Join");
        GridBagConstraints gbc_but_join = new GridBagConstraints();
        gbc_but_join.insets = new Insets(0,0,5,0);
        gbc_but_join.gridx = 1;
        gbc_but_join.gridy = 4;
        gbc_but_join.fill = GridBagConstraints.CENTER ;
        
        panel_1.add(but_join, gbc_but_join);
        
        but_join.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int port =(int) ((Math.random()*2000)+2000);
				System.out.println(port);
				// can only join one time successfully.
				if (drawer.remoteManager != null) {
					// 
					JOptionPane.showMessageDialog(frame, "you have join one! do not join again.");
					
					return;
				}
				
				// regular expression check.
				userName = join_area.getText();
				System.out.println("---------");
				System.out.println(userName);
				if (userName.equals("")) {
					JOptionPane.showMessageDialog(frame, "please input your name!");
					return;
				}
				
				// remove space
				String cleanedUserName = userName.replaceAll("\\s+","_");
				// remove non english letter.
				cleanedUserName = userName.replaceAll("\\W+","_");
				
				try {
					drawer.registerToManager(port,manageIP,managePort, cleanedUserName);
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(frame, "connection error, try later!");
					return;
//						e1.printStackTrace();
				}
				try {
					drawer.InitialDrawing();
				} catch (Exception e2) {
					System.out.println("空指针，因为没有连接上");
				}
			}
		});
       
       JSlider slider = new JSlider();  
      
       GridBagConstraints gbc_sl = new GridBagConstraints();
       gbc_sl.insets = new Insets(0,0,5,0);
       gbc_sl.gridx = 0;
       gbc_sl.gridy = 2; 
       gbc_sl.gridwidth = 2;
       panel_1.add(slider,gbc_sl);
       //设置绘制刻度  
        slider.setPaintTicks(true);  
        //设置主、次刻度的间距  
        slider.setMajorTickSpacing(20);  
        slider.setMinorTickSpacing(5);
        slider.setValue(25);
        //设置绘制刻度标签，默认绘制数值刻度标签  
        slider.setPaintLabels(true); 
        JLabel jl1 = new JLabel("weight/Font size");
        
        GridBagConstraints gbc_jl1 = new GridBagConstraints();
        gbc_jl1.insets = new Insets(0,0,5,0);
        gbc_jl1.gridx = 0;
        gbc_jl1.gridy = 1;
        gbc_jl1.gridwidth = 2;
        panel_1.add(jl1,gbc_jl1);
//        addSlider(slider, "Bold Chooser"); 
        slider.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				//System.out.println(slider.getValue());
				
				drawer.stroke=slider.getValue()/5;
				
			}
		});
        

        JPanel panel = new JPanel();


        panel.setPreferredSize(new Dimension(100,700));


        panel.setBackground(Color.LIGHT_GRAY);
        GridBagConstraints gbc_panel = new GridBagConstraints();
        gbc_panel.gridheight = 7;
        gbc_panel.gridwidth = 2;
        gbc_panel.insets = new Insets(0, 0, 5, 5);
        gbc_panel.fill = GridBagConstraints.BOTH;
        gbc_panel.gridx = 0;
        gbc_panel.gridy = 1;
        frame.getContentPane().add(panel, gbc_panel);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0};
        gbl_panel.rowHeights = new int[]{0,0,0,0,0,0,0,0};
        gbl_panel.columnWeights = new double[]{0.1,Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.1,0.1, 0.1,0.1,0.1,0.1,0.1,0.1,Double.MIN_VALUE};
        panel.setLayout(gbl_panel);
        
        
        
        // 添加图形按钮
//        String[] shapeArray = { "Select","Line", "Rectangle", "Oval", "Text", "Pencil", "Eraser", "Clear" };
        for (int i = 0; i < shapeArray.length; i++) {
            // 创建图形按钮
            JButton jbu1 = new JButton(shapeArray[i]);
            jbu1.setIcon(new ImageIcon(ClientUI.class.getResource("/images/" + shapeArray[i] + ".png")));
            GridBagConstraints gbc_jbu1 = new GridBagConstraints();
            gbc_jbu1.insets = new Insets(0,0,5,0);
            gbc_jbu1.gridx = 0;
            gbc_jbu1.gridy = i;
            
//            jbu1.setIcon(images[i]);
            // 设置按钮大小
//			jbu1.setPreferredSize(new Dimension(100, 40));
            // 将按钮添加到jp2容器中
            panel.add(jbu1,gbc_jbu1);
            // 给按钮注册监听器
            jbu1.addActionListener(drawer);
        }


//	  //yao  添加button send按钮
//        JButton sendButton = new JButton("send");  //按钮，单击响应事件，打开文件选择器
//	  
//        GridBagConstraints gbc_sendButton = new GridBagConstraints();
//        gbc_sendButton.insets = new Insets(0,0,5,0);
//        gbc_sendButton.gridx = 0;
//        gbc_sendButton.gridy = 0;
//        panel_3.add(sendButton, gbc_sendButton);
        drawer.sendButton = but_send;
        drawer.jtext_chat= jtext_chat;
        drawer.record = record; 
//	  //yao 添加chat box按钮
//        chatBox = new JTextArea("hello");
//		GridBagConstraints gbc_textArea_1 = new GridBagConstraints();
//		chatBox.setBackground(Color.red);
//		gbc_textArea_1.insets = new Insets(0, 0, 5, 5);
////		gbc_textArea_1.fill = GridBagConstraints.BOTH;
//		gbc_textArea_1.gridx = 1;
//		gbc_textArea_1.gridy = 1;
////		gbc_textArea_1.weightx = 2;
////		gbc_textArea_1.weighty = 3;
//		panel_3.add(chatBox, gbc_textArea_1);
//		drawer.chatBox = chatBox;
//		// yao 添加点击发送事件
        
        but_send.addActionListener(drawer);
		
        

        JMenuBar menuBar = new JMenuBar();
        GridBagConstraints gbc_menuBar = new GridBagConstraints();
        gbc_menuBar.gridwidth = 15;
        gbc_menuBar.gridheight = 1;
        gbc_menuBar.insets = new Insets(0, 0, 5, 5);
        gbc_menuBar.fill = GridBagConstraints.BOTH;
        gbc_menuBar.gridx = 0;
        gbc_menuBar.gridy = 0;
        frame.getContentPane().add(menuBar, gbc_menuBar);

//        JMenu fileMenu = new JMenu("File");
        JMenu editMenu = new JMenu("Edit");
        JMenu fontMenu = new JMenu("Font");
//        JMenu sizeMenu = new JMenu("Size");

//        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        menuBar.add(fontMenu);
//        menuBar.add(sizeMenu);

//        JMenuItem newMenuItem = new JMenuItem("new");
//        newMenuItem.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//            	
//            	JFileChooser Jfc = new JFileChooser();
//                Jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
//                Jfc.showDialog(new JLabel(), "choose directory");
//                File file= Jfc.getSelectedFile().getAbsoluteFile();
//                String str=JOptionPane.showInputDialog(Jfc,"Enter file name","File name",JOptionPane.PLAIN_MESSAGE);
////                System.out.println(file.getAbsolutePath()+"/"+str+".json");
//                savePath = file.getAbsolutePath()+"/"+str;
//                //System.out.println("-----------------");
//                //System.out.println(savePath);
////                drawer.shapes.saveinFile(file.getAbsolutePath()+"/"+str);
//            }
//        });
//        
//        JMenuItem openMenuItem = new JMenuItem("open");
//        openMenuItem.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                JFileChooser Jfc = new JFileChooser();
//                Jfc.setFileSelectionMode(JFileChooser.FILES_ONLY );
//                Jfc.showDialog(new JLabel(), "choose file");
//                File file= Jfc.getSelectedFile();
//                System.out.println(file.getAbsolutePath());
//                drawer.shapes = Shapes.recoverFromFile(file.getAbsolutePath());
//                drawer.repaint(ClientUI.this);
//            }
//        });
//        JMenuItem saveMenuItem = new JMenuItem("save");
//        saveMenuItem.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//            	
//            	if (savePath != null) {
//            		drawer.shapes.saveinFile(savePath);
//            	}else {
//            		drawer.shapes.saveinFile("autosave");
//            	}
//            }
//        });
//        JMenuItem saveInMenuItem = new JMenuItem("save in");
//        saveInMenuItem.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                JFileChooser Jfc = new JFileChooser();
//                Jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
//                Jfc.showDialog(new JLabel(), "choose directory");
//                File file= Jfc.getSelectedFile().getAbsoluteFile();
//                String str=JOptionPane.showInputDialog(Jfc,"Enter file name","File name",JOptionPane.PLAIN_MESSAGE);
//                System.out.println(file.getAbsolutePath()+"/"+str+".json");
//                drawer.shapes.saveinFile(file.getAbsolutePath()+"/"+str);
//            }
//        });
//        JMenuItem saveAsMenuItem = new JMenuItem("save as");
//        saveAsMenuItem.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//            	try {
//            		JFileChooser Jfc = new JFileChooser();
//                    Jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
//                    Jfc.showDialog(new JLabel(), "choose directory");
//                    File file= Jfc.getSelectedFile().getAbsoluteFile();
//                    String str=JOptionPane.showInputDialog(Jfc,"Enter file name","File name",JOptionPane.PLAIN_MESSAGE);
//                    Dimension imageSize = ClientUI.this.getSize();
//                    BufferedImage image = new BufferedImage(imageSize.width,
//                            imageSize.height, BufferedImage.TYPE_INT_ARGB);
//                    Graphics2D g = image.createGraphics();
//                    ClientUI.this.paint(g);
//                    drawer.shapes.drawShape(g);
//                    g.dispose();
//                    System.out.print(file.getAbsolutePath());
//                    ImageIO.write(image, "png", new File(file.getAbsolutePath()+"/"+str+".png"));
//            	}catch (Exception ee) {
//            		ee.printStackTrace();
//				}
//            	
////                JFileChooser Jfc = new JFileChooser();
////                Jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY );
////                Jfc.showDialog(new JLabel(), "choose directory");
////                File file= Jfc.getSelectedFile().getAbsoluteFile();
////                String str=JOptionPane.showInputDialog(Jfc,"Enter file name","File name",JOptionPane.PLAIN_MESSAGE);
////                Dimension imageSize = ClientUI.this.getSize();
////                BufferedImage image = new BufferedImage(imageSize.width,
////                        imageSize.height, BufferedImage.TYPE_INT_ARGB);
////                Graphics2D g = image.createGraphics();
////                ClientUI.this.paint(g);
////                drawer.shapes.drawShape(g);
////                g.dispose();
////                try {
////                    ImageIO.write(image, "png", new File(file.getAbsolutePath()+"/"+str+".png"));
////                } catch (IOException ioe) {
////                    ioe.printStackTrace();
////                }
//            }
//        });
//        JMenuItem exitMenuItem = new JMenuItem("exit");
//        fileMenu.add(newMenuItem);
//        fileMenu.add(openMenuItem);
//        fileMenu.add(saveMenuItem);
//        fileMenu.add(saveInMenuItem);
//        fileMenu.add(saveAsMenuItem);
//        // 添加一条分割线
//        fileMenu.addSeparator();
//        fileMenu.add(exitMenuItem);

        JMenuItem delMenuItem = new JMenuItem("delete");
        delMenuItem.addActionListener(e -> drawer.delSelected());
        JMenuItem undoMenuItem = new JMenuItem("undo");
        undoMenuItem.addActionListener(e->drawer.undo());
        //JMenuItem pasteMenuItem = new JMenuItem("paste");

        editMenu.add(delMenuItem);
        editMenu.add(undoMenuItem);

        String[] fontList = {"Times New Roman","Helvetica","Arial","Centaur", "Garamond",
                "Caslon", "Baskerville", "Didot", "Futura", "Optima","Myriad", "Calibri",
                "Trajan","Bodoni","Bickham Script Pro", "Frutiger"};
        for (String i : fontList){
            JMenuItem item = new JMenuItem(i);
            //
            item.setFont(new Font(i,0, 15));
            item.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    drawer.currentFont = i;
                }
            });
            fontMenu.add(item);
        }
        
    }
    
//    //定义一个方法，用于将滑动条添加到容器中  
//    public void addSlider(JSlider slider, String description)  
//    {          
//        slider.addChangeListener(listener);  
//        Box box = new Box(BoxLayout.X_AXIS);  
//        box.add(new JLabel(description + "："));  
//        box.add(slider);  
//        sliderBox.add(box);  
//    }  
    
    // 重写父类方法
    public void paint(Graphics g) {
        super.paint(g);
        //遍历图形数组，重绘图形
        drawer.repaint(this);
        /*
        shapeParameter = drawer.shapes;
        for (int i = 0; i < shapeParameter.size(); i++) {
            Shape shape = shapeParameter.get(i);
            if (shapeParameter.get(i) != null) {
                shape.drawShape(this);
            }
        }*/
    }
}


